Database Credentials
All database related info is placed in application.properties file.
#Sample API Application
-Database name =sampleAPI

-password=absa

-user=Absa

#Assumptions Made
The file.txt file is received under *src\main\resources\extractedRecords\\* from the Host Application.

#To run via cmd or powershell or bash
-mvn spring-boot:run

#port
listening on "http://localhost:9091"
